import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MycompsComponent } from './mycomps.component';

describe('MycompsComponent', () => {
  let component: MycompsComponent;
  let fixture: ComponentFixture<MycompsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MycompsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MycompsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
