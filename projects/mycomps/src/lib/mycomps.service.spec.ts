import { TestBed, inject } from '@angular/core/testing';

import { MycompsService } from './mycomps.service';

describe('MycompsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MycompsService]
    });
  });

  it('should be created', inject([MycompsService], (service: MycompsService) => {
    expect(service).toBeTruthy();
  }));
});
