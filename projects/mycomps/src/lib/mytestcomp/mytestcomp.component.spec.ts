import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MytestcompComponent } from './mytestcomp.component';

describe('MytestcompComponent', () => {
  let component: MytestcompComponent;
  let fixture: ComponentFixture<MytestcompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MytestcompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MytestcompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
