import { NgModule } from '@angular/core';
import { MycompsComponent } from './mycomps.component';
import { MytestcompComponent } from './mytestcomp/mytestcomp.component';
import { MockUtilModule } from './mockutil/mock-util.module';

@NgModule({
  imports: [
    MockUtilModule
  ],
  declarations: [MycompsComponent, MytestcompComponent],
  exports: [MycompsComponent, MytestcompComponent]
})
export class MycompsModule { }
