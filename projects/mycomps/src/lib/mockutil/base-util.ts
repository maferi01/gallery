
export abstract class BaseUtil {
  protected log;
  constructor() {
    this.log = {
      info(msg) {
        console.log(msg);
      }
    };
    this.log.info('Class ' + this.constructor.name + ' created');
  }
}
