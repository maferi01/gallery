import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';

import { catchError, map, tap, refCount, publishReplay, delay, retry, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { MockDataService } from './mock-data.service';
import { IMockData } from './imock-data.model';

import { Observable, of } from 'rxjs';
import { BaseUtil } from './base-util';


@Injectable()
export class AppInterceptorMock extends BaseUtil implements HttpInterceptor {
  constructor(private mockSrv: MockDataService) {
    super();
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
   const mock: IMockData = this.mockSrv.getRequestMock(req);

   this.log.info('enter app interceptor MOCK***********');

   return of(null).pipe(
      mergeMap(() => {
        if (mock) {
          return this.mockSrv.getResponseMock(mock);
        }
        const changedReq = req.clone({
          headers: req.headers.set('My-Header', 'MyHeaderValue')
        });
        return next.handle(changedReq);
      })
      , materialize()
      , delay(500)
      , dematerialize()
    );


  }
}
