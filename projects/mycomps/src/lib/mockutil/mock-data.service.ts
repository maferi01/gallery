import { HttpErrorResponse, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Inject, Injectable, InjectionToken } from '@angular/core';
import { of, throwError, Observable } from 'rxjs';

import { BaseUtil } from './base-util';
import { IHeader, IMock, IMockData, IParam } from './imock-data.model';

export const TOKEN_MOCK = new InjectionToken('mockdata');
// import 'rxjs/add/observable/throw';
@Injectable()
export class MockDataService extends BaseUtil {

  constructor(@Inject(TOKEN_MOCK) private mockData: IMock) {
    super();
  }


  getRequestMock(req: HttpRequest<any>): IMockData {
    return this.mockData.mocks.find((data: IMockData) => {
      // check if the method and url and params and match params
      if (
        req.method === data.type &&
        req.url.includes(data.url) &&
        this.matchHeaders(data, req) &&
        this.matchParams(data, req) &&
        this.matchBody(data, req)
      ) {
        if (data.name) {
          this.log.info('---mock data matched ' + data.name);
        }
        return true;
      }
      return false;
    });
  }

  getRequestMockData(req: HttpRequest<any>, mockData: IMock): IMockData {
    if (!mockData) {
      return null;
    }

    return mockData.mocks.find((data: IMockData) => {
      // check if the method and url and params and match params
      if (
        this.matchMethod(data, req) &&
        this.matchUrl(data, req) &&
        this.matchHeaders(data, req) &&
        this.matchParams(data, req) &&
        this.matchBody(data, req)
      ) {
        if (data.name) {
          this.log.info('---mock data matched ' + data.name);
        }
        return true;
      }
      return false;
    });
  }

  private matchMethod(data: IMockData, req: HttpRequest<any>): boolean {
    if (data.type) {
      return req.method === data.type;
    }
    return true;
  }

  private matchUrl(data: IMockData, req: HttpRequest<any>): boolean {
    if (data.url) {
      return req.url.includes(data.url);
    }
    return true;
  }

  private matchParams(data: IMockData, req: HttpRequest<any>): boolean {
    if (data.params) {
      return data.params.every(
        (param: IParam) => req.params[param.key] === param.value || (req.params.get && req.params.get(param.key) === param.value)
      );
    }
    return true;
  }
  private matchHeaders(data: IMockData, req: HttpRequest<any>): boolean {
    if (data.headersRequest) {
      return data.headersRequest.every((header: IHeader) => req.headers.get(header.key) === header.value);
    }
    return true;
  }

  private matchBody(data: IMockData, req: HttpRequest<any>): boolean {
    if (data.body) {
      const res = objectEquals(data.body, req.body);
      this.log.info(` ${data.name} -compare res=${res} body req ${JSON.stringify(req.body)}`);
      // return JSON.stringify(data.body)==JSON.stringify(req.body);
      return res;
    }
    return true;
  }

  getResponseMock(data: IMockData):Observable<any> {
    const resp = this.getResponseMockObj(data);
    if (resp instanceof HttpErrorResponse) {
      return throwError(resp);
    } else {
      return of(resp);
    }
  }

  getResponseMockObj(data: IMockData): HttpErrorResponse | HttpResponse<object> {
    if (!data.resposeStatus) {
      data.resposeStatus = 200;
    }
    let headers = new HttpHeaders();
    if (data.headersResponse) {
      data.headersResponse.forEach(header => (headers = headers.append(header.key, header.value)));
    }
    if (data.resposeStatus !== 200) {
      return new HttpErrorResponse({
        status: data.resposeStatus,
        headers: headers,
        error: data.error,
        statusText: data.statusText
      });
    }
    return (
      new HttpResponse({
        status: data.resposeStatus,
        body: data.response ? data.response : null,
        headers: headers
      })
    );
  }
}

function objectEquals(x, y) {
  // if both are function
  if (x instanceof Function) {
    if (y instanceof Function) {
      return x.toString() === y.toString();
    }
    return false;
  }
  if (x === null || x === undefined || y === null || y === undefined) {
    return x === y;
  }
  if (x === y || x.valueOf() === y.valueOf()) {
    return true;
  }

  // if one of them is date, they must had equal valueOf
  if (x instanceof Date) {
    return false;
  }
  if (y instanceof Date) {
    return false;
  }

  // if they are not function or strictly equal, they both need to be Objects
  if (!(x instanceof Object)) {
    return false;
  }
  if (!(y instanceof Object)) {
    return false;
  }

  const p = Object.keys(x);
  return p.every(function(i) {
    return objectEquals(x[i], y[i]);
  });

  /* return Object.keys(y).every(function (i) { return p.indexOf(i) !== -1; }) ?
          p.every(function (i) { return objectEquals(x[i], y[i]); }) : false; */
}
