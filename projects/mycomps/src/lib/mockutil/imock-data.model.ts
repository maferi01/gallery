export interface IMock {
  mocks: IMockData[];
}

export interface IMockData {
  name?: string;
  // request data
  type?: string;
  url?: string;
  body?: any;
  params?: IParam[];
  headersRequest?: IHeader[];
  // response data
  response?: any;
  responseFile?: string;
  resposeStatus?: number;
  headersResponse?: IHeader[];
  statusText?: string;
  error?: any;
}

export interface IParam {
  key: string;
  value: string;
}
export interface IHeader {
  key: string;
  value: string;
}
