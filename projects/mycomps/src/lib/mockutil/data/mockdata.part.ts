import { IMock } from '../imock-data.model';

export const mockDataPart: IMock = {mocks: [
  { name: 'test info mock',
    response: {name:'artur',age:40},
    type: 'GET',
    url: 'user',
    resposeStatus: 200
  }
]};
