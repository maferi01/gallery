import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppCoreInterceptor } from './app.core.interceptor';
import { AppInterceptorMock } from './app.interceptor';
import { MockDataService } from './mock-data.service';

@NgModule({
  imports: [],
  declarations: [],
  providers: [
    MockDataService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppCoreInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppInterceptorMock,
      multi: true
    }
  ]
})
export class MockUtilModule {
  constructor(){
    console.log('enter mockkkk module')
  }
}
