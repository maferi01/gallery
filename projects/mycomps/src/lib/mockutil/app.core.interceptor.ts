import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { of, throwError} from 'rxjs';

import { catchError, map, tap, refCount, publishReplay, delay, retry } from 'rxjs/operators';
import { MockDataService } from './mock-data.service';
import { BaseUtil } from './base-util';


@Injectable()
export class AppCoreInterceptor extends BaseUtil implements HttpInterceptor {
  constructor() {
    super();
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
         const changedReq = req.clone({
            headers: req.headers.set('My-Header', 'MyHeaderValue')
          });
          this.log.info('pass intercept coreee');
          return next.handle(changedReq)
          .pipe(tap((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
              const r = event as HttpResponse<any>;
              this.log.info('url resp**===' + r.url);
              this.log.info('status resp**===' + r.status);
              this.log.info('headers resp**===' + JSON.stringify(r.headers));
           }
          }, (error: HttpErrorResponse) => {
            this.log.info('headers error resp**==' + JSON.stringify(error.headers));
            this.log.info('error resp**==' + JSON.stringify(error.error));
            this.log.info('error core status***' + error.status);
          })
          , catchError((err) => {
            this.log.info('error cath core***' + err);
            return throwError(err);
          })
        );

        /*   catch((err)=>{
            console.log('error cath core***'+err);
            return Observable.throw(err);
          }); */

        }

}
