import { Directive, HostListener, Input, Renderer2, ElementRef, OnInit } from '@angular/core';

/**
 * Directiva para pasar al siguiente input al introducir un caracter y pasar al anterior al borrar un caracter
 */
@Directive({
  selector: '[appChilddir]'
})
export class ChilddirDirective implements OnInit {
  @Input()
  mode: string;
  @Input()
  enablePrev: boolean;
  @Input()
  enableNext: boolean;
  @Input()
  idFocus: string;
  @Input()
  nextTarget: string;
  @Input()
  prevTarget: string;

  constructor(public renderer: Renderer2, public hostElement: ElementRef) {
    this.enableNext = true;
    this.enablePrev = true;
  }

  ngOnInit(): void {
    if (this.idFocus) {
      this.renderer.setAttribute(this.hostElement.nativeElement, 'idfocus', this.idFocus);
    }
  }

  getIndexForm(input: HTMLInputElement): number {
    const controls = this.getAllControls(input);
    for (let i = 0; i < controls.length; i++) {
      if (input === controls[i]) {
        return i;
      }
    }
  }

  getAllControls(input: HTMLInputElement) {
    const allControls = [];
    return input.ownerDocument.querySelectorAll('input');
  }

  getNextInput(input: HTMLInputElement): HTMLInputElement {
    const controls = this.getAllControls(input);
    for (let i = this.getIndexForm(input) + 1; i < controls.length; i++) {
      const formElem = controls[i] as HTMLInputElement;
      if (formElem.tagName.toLowerCase() === 'input' && !formElem.disabled &&
      (this.nextTarget == null || formElem.getAttribute('idfocus') === this.nextTarget )) {
        return formElem;
      }
    }
    return null;
  }

  getPrevInput(input: HTMLInputElement): HTMLInputElement {
    const controls = this.getAllControls(input);
    for (let i = this.getIndexForm(input) - 1; i >= 0; i--) {
      const formElem = controls[i] as HTMLInputElement;
      if (formElem.tagName.toLowerCase() === 'input' && !formElem.disabled &&
      (this.prevTarget == null || formElem.getAttribute('idfocus') === this.prevTarget )) {
        return formElem;
      }
    }
    return null;
  }

  @HostListener('keyup', ['$event'])
  onkeyup(e: KeyboardEvent) {
    let myTarget: any;
    if (e.target) {
      myTarget = e.target;
    } else if (e.srcElement) {
      myTarget = e.srcElement;
    }
    const maxLength = parseInt(myTarget.attributes.maxlength.value, 10);
    const inputLength = myTarget.value.length;

    if (inputLength >= maxLength) {
      if (!this.enableNext) {
        return;
      }
      const next = this.getNextInput(myTarget);
      if (next) {
        next.focus();
      }
    } else if (inputLength === 0 && e.keyCode === 8) {
      if (!this.enablePrev) {
        return;
      }
      const next = this.getPrevInput(myTarget);
      if (next) {
        next.focus();
      }
    }
    // console.log('input found ' + this.getIndexForm(myTarget));
  }
}
