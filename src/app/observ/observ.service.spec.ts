import { TestBed, inject } from '@angular/core/testing';

import { ObservService } from './observ.service';

describe('ObservService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ObservService]
    });
  });

  it('should be created', inject([ObservService], (service: ObservService) => {
    expect(service).toBeTruthy();
  }));
});
