import { Component, OnInit } from "@angular/core";
import { ObservService } from "./observ.service";
import {
  tap,
  concatMap,
  mergeMap,
  switchMap,
  takeUntil,
  delay,
  filter,
  first,
  map
} from "rxjs/operators";
import {
  Observable,
  timer,
  merge,
  Subscription,
  concat,
  BehaviorSubject,
  of,
  interval
} from "rxjs";
import { pipe } from "@angular/core/src/render3/pipe";
import { SerchObs } from "../child-subs/child-subs.component";
import { UserService, IUser } from "./user.service";

@Component({
  selector: "app-observ",
  templateUrl: "./observ.component.html",
  styleUrls: ["./observ.component.scss"],
  providers: [ObservService]
})
export class ObservComponent implements OnInit {
  valPas;
  subs: Subscription;
  subject: BehaviorSubject<any>;
  obsInfo: Observable<{ name: string }>;
  name;
  conA: number = 0;
  conB: number = 0;
  susA: number = 0;
  susB: number = 0;

  obsSerach: Observable<SerchObs>;
  userObs: Observable<IUser>;


  constructor(private serv: ObservService,private userServ:UserService) {
    this.obsInfo = of({ name: "artur" }).pipe(
      delay(3000),
      tap(v => console.log("info ", v))
    );
    this.getUser();
  }

  ngOnInit() {
    const searchFu: SerchObs = (par) => {
      console.log('enter function serach ',par)
      return interval(par).pipe(
        tap(t=> console.log('timer intreval',t)),
        map(t=> 'interval time '+t)

      )
    };
    this.obsSerach= of(searchFu).pipe(
      tap(v=> console.log('valor obsSerach ',v))
    );
  }

  clA() {
    this.susA++;
    const sus = this.susA;
    this.serv.getData("a").subscribe(d => console.log(d + "subs " + sus));
  }
  clB() {
    this.susB++;
    const sus = this.susB;
    this.serv.getData("b").subscribe(d => console.log(d + "subs " + sus));
  }

  nextA() {
    this.serv.setData("a", "valor a" + this.conA);
    this.conA++;
  }
  nextB() {
    this.serv.setData("b", "valor b" + this.conB);
    this.conB++;
  }

  pas() {
    console.log(this.valPas);
  }

  join() {
    const obs1 = timer(1000, 2000).pipe(
      tap(t => console.log("emit from obs 1", t))
      // takeUntil(timer(10000))
    );
    const obs2 = timer(1000, 7000).pipe(
      tap(t => console.log("emit from obs 2", t))
    );
    const obs3 = timer(1000, 11000).pipe(
      tap(t => console.log("emit from obs 3", t))
    );
    this.subs = obs1
      .pipe(
        mergeMap(t => {
          return obs2.pipe(
            tap(t2 => console.log("tap obs2 ** ob1=" + t + " ob2=" + t2))
          );
        })
      )
      .subscribe();

    //this.subs=merge(obs1,obs2,obs3).subscribe(t=>console.log('from merge obs ********************************* ',t));
  }
  endjoin() {
    this.subs.unsubscribe();
  }

  getOnlyOne(): Observable<any> {
    if (this.subject == null) {
      this.subject = new BehaviorSubject<any>({});
      return this.obsInfo.pipe(
        mergeMap(d => {
          this.subject.next(d);
          return this.subject.asObservable();
        })
      );
    }
    return this.subject.asObservable().pipe(
      filter(d => d.name != null),
      first()
    );
  }

  getInfo() {
    this.getOnlyOne().subscribe(d => {
      this.name = d.name;
      console.log("get info ", d);
    });
  }

  getUser(){
   this.userServ.getUser().subscribe(u=> console.log(u.name, u));
  }

}
