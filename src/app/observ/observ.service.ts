import {
  mergeMap,
  last,
  withLatestFrom,
  combineAll,
  distinct
} from "rxjs/operators";
import { Injectable } from "@angular/core";
import {
  Subject,
  BehaviorSubject,
  ReplaySubject,
  Observable,
  of,
  interval,
  combineLatest
} from "rxjs";
import { filter, map, takeLast } from "rxjs/operators";

interface KeyData {
  key: string;
  data: any;
}
@Injectable({
  providedIn: "root"
})
export class ObservService {
  subject: ReplaySubject<KeyData>;
  subjectBe: BehaviorSubject<KeyData>;

  constructor() {
    this.subject = new ReplaySubject<any>();
    this.subjectBe = new BehaviorSubject<any>({});
    this.subjectBe.asObservable().subscribe(this.subject); //link subjects
  }

  setData(key: string, val) {
    const ob: KeyData = { key: key, data: val };
    this.subjectBe.next(ob); // emits all value
    console.log("next ", ob);
  }

  getData(key: string): Observable<any> {
    const obs = this.subject.asObservable().pipe(
      filter(k => k.key === key) //filter values by key
    );

    return this.subjectBe.pipe(
      withLatestFrom(obs), //only last from subject
      map(d => d[1].data),
      distinct()
    );
  }
}
