import { Directive, ElementRef, HostListener, Renderer } from '@angular/core';

@Directive({
  selector: '[appSecureText]'
})
export class SecureTextDirective {

constructor(elRef: ElementRef,renderer: Renderer) {
    elRef.nativeElement.style.color = 'red';
    //(elRef.nativeElement as HTMLInputElement).type="text";
    renderer.setElementAttribute(elRef.nativeElement,'type','text');
    (elRef.nativeElement as HTMLInputElement).className="secure-text";
    var events = 'cut copy paste';
    events.split(' ').forEach(e =>
    renderer.listen(elRef.nativeElement, e, (event) => {
      console.log('in change InputTextFilterDirective');
      event.preventDefault();
      })
    );
  }
  // @HostListener('copy')
  // onCopy($event) {
  //   console.log('in change InputTextFilterDirective');
  //   $event.preventDefault();
  // }
}
