import { InjectionToken } from '@angular/core';
import { IMock } from 'projects/mycomps/src/lib/mockutil/imock-data.model';



export const mockDataPart: IMock = {mocks: [
  { name: 'test info mock',
    response: {name:'artur',age:40},
    type: 'GET',
    url: 'user',
    resposeStatus: 200
  }
]};
