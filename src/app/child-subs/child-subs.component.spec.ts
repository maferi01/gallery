import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildSubsComponent } from './child-subs.component';

describe('ChildSubsComponent', () => {
  let component: ChildSubsComponent;
  let fixture: ComponentFixture<ChildSubsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildSubsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildSubsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
