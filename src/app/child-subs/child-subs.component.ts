import { Component, OnInit, Input } from "@angular/core";
import { Observable } from "rxjs";
import { mergeMap, finalize, tap, switchMap, debounce } from "rxjs/operators";
import { FormControl } from "@angular/forms";

export type SerchObs = (par: number) => Observable<string>;

@Component({
  selector: "app-child-subs",
  templateUrl: "./child-subs.component.html",
  styleUrls: ["./child-subs.component.scss"]
})
export class ChildSubsComponent implements OnInit {
  @Input() obsChild: Observable<SerchObs>;
  par1: number;
  output: string;
  obsout: Observable<string>;
  searchIn = new FormControl("searchIn");

  constructor() {}

  ngOnInit() {
    this.obsout=this.searchIn.valueChanges.pipe(
    //  debounce(),
      switchMap(v => this.search(v)))
  }

  search(par) {
    return this.obsChild.pipe(
      mergeMap(searchFunc => searchFunc(par)),
      finalize(() => console.log("final obs"))
    );
  }
}
