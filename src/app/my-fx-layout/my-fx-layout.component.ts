import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-fx-layout',
  templateUrl: './my-fx-layout.component.html',
  styleUrls: ['./my-fx-layout.component.scss']
})
export class MyFxLayoutComponent implements OnInit {

  layoutCont;

  constructor() {
    this.layoutCont="column";
   }

  ngOnInit() {
  }

  chRow(){
    this.layoutCont="row";
  }
  chCol(){
    this.layoutCont="column";
  }
  wrap(){
    this.layoutCont+=' wrap';
  }

  cl(){
    console.log('enterrr clickkkk');
  }
  cl2(){
    console.log('enterrr clickkkk222222222222');
  }


}
