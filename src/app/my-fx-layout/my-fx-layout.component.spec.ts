import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyFxLayoutComponent } from './my-fx-layout.component';

describe('MyFxLayoutComponent', () => {
  let component: MyFxLayoutComponent;
  let fixture: ComponentFixture<MyFxLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyFxLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyFxLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
