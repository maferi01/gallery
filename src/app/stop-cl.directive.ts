import { Directive } from '@angular/core';
import { HostListener } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Directive({
  selector: '[appStopCl]'
})
export class StopClDirective {

  constructor() { }
  @Output()
  public appStopCl = new EventEmitter();

  @HostListener('document:click', ['$event'])
  public onClick(e:MouseEvent) {
    console.log('clickkk directivee ----------------------');
    e.preventDefault();
    this.appStopCl.emit(null);
  }

}
