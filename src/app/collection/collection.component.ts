import { Component, OnInit } from '@angular/core';
import { Item } from './item';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {

  items: Item[]=[];
  constructor() {
    for(let i=0;i<30;i++){
      this.items.push({name: `name${i}`, description: `description${i}`, img: `img${i}`});
    }

   }

  ngOnInit() {
  }

}
