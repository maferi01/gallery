import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeconPageComponent } from './secon-page.component';

describe('SeconPageComponent', () => {
  let component: SeconPageComponent;
  let fixture: ComponentFixture<SeconPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeconPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeconPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
