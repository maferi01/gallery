import { PositionLayout } from './position.enum';

export interface IProps {}

export interface IPropsHeader {
  height: string;
}
export interface IPropsFooter {
  height: string;
}
export interface IPropsMain {}
export interface IPropsAside {
  height?: string;
  width?: string;
  position: PositionLayout;
}
export interface IPropsNav {
  height?: string;
  width?: string;
  position: PositionLayout;
  responsive?: boolean;
}
