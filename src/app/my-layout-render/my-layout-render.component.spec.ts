import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyLayoutRenderComponent } from './my-layout-render.component';

describe('MyLayoutRenderComponent', () => {
  let component: MyLayoutRenderComponent;
  let fixture: ComponentFixture<MyLayoutRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyLayoutRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyLayoutRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
