import { PositionLayout } from './position.enum';
import { IPropsHeader, IPropsFooter, IPropsAside, IPropsNav } from './iprops';
import { ChildComponent } from './../child/child.component';
import {
  Component,
  OnInit,
  TemplateRef,
  Input,
  ContentChild,
  AfterContentInit,
  ContentChildren,
  QueryList,
  ViewChildren,
  AfterViewInit,
  ViewChild,
  ChangeDetectorRef
} from '@angular/core';
import { ChilddirDirective } from '../childdir.directive';

@Component({
  selector: 'app-my-layout-render',
  templateUrl: './my-layout-render.component.html',
  styleUrls: ['./my-layout-render.component.scss']
})
export class MyLayoutRenderComponent implements OnInit, AfterContentInit, AfterViewInit {
  @Input()
  @ContentChild('main')
  mainRef: TemplateRef<any>;
  @ContentChild('header')
  headerRef: TemplateRef<any>;
  @ContentChild('footer')
  footerRef: TemplateRef<any>;
  @ContentChild('aside')
  asideRef: TemplateRef<any>;
  @ContentChild('nav')
  navRef: TemplateRef<any>;

  @Input()
  propsHeader: IPropsHeader;
  @Input()
  propsFooter: IPropsFooter;
  @Input()
  propsAside: IPropsAside;
  @Input()
  propsNav: IPropsNav;
  PositionLayout = PositionLayout;

  @ContentChildren(ChildComponent, { descendants: true })
  childs: QueryList<ChildComponent>;

  // @ViewChild('inner') inner: ChildComponent;

  constructor(private cd: ChangeDetectorRef) {
    this.defaultProps();
  }

  ngOnInit() {
    //   console.log('legth of childs ' + this.childs.length);
  }

  defaultProps() {
    this.propsHeader = { height: '70px' };
    this.propsFooter = { height: '50px' };
    this.propsAside = { height: '80px', position: PositionLayout.RIGHT };
    this.propsNav = { width: '80px', position: PositionLayout.LEFT };
  }

  cl() {
    console.log('enterrrrr cl layout render');
  }
  ngAfterViewInit(): void {
    //  this.cd.detectChanges();
    //   console.log(this.inner.hello);
    // this.childs.notifyOnChanges();
    console.log('legth of childs ****' + this.childs.length);

    //    this.childs.changes.subscribe(d => console.log('legth of childs *** ' + this.childs.length));
  }
  ngAfterContentInit(): void {
    //  this.childs.notifyOnChanges();
    // this.cd.detectChanges();
    // console.log('legth of childs ' + this.childs.length);
    this.childs.changes.subscribe(d => {
      console.log('legth of childs susscribe ' + this.childs.length);
      if (this.hasChild) {
        this.childs.first.hello = 'change from layoutt xxxx';
      }
    });
  }

  get hasChild() {
    return this.childs.length > 0;
  }

  get flexNav() {
    if (this.propsNav.position === PositionLayout.TOP || this.propsNav.position === PositionLayout.BOTTOM) {
      return '100';
    } else {
      return this.propsNav.width;
    }
  }
}
