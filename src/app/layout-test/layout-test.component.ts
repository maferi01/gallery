import { PositionLayout } from './../my-layout-render/position.enum';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout-test',
  templateUrl: './layout-test.component.html',
  styleUrls: ['./layout-test.component.scss']
})
export class LayoutTestComponent implements OnInit {
  PositionLayout = PositionLayout;

  cad: any[];
  cad2: any[];

  all: object = {};
  all2: object = {};


  constructor() {
    this.cad = [
      { val: 'aa', enab: true },
      { val: 'bb', enab: false },
      { val: 'cc', enab: true , idFocus: 'f-cc', next: 'f-cc2'},
      { val: 'dd', enab: false },
      { val: 'ee', enab: true },
      { val: 'mm', enab: true, idFocus: 'f-mm'},
      { val: 'ee', enab: true }
    ];
    this.cad2 = [
      { val: 'aa', enab: true },
      { val: 'bb', enab: false },
      { val: 'cc', enab: true , idFocus: 'f-cc2', back: 'f-cc'},
      { val: 'dd', enab: false, idFocus: 'f-dd2' },
      { val: 'ee', enab: true , back: 'f-mm'}
    ];

    this.all = Array(this.cad.length).fill(1);
    this.all2 = Array(this.cad2.length).fill(1);

  }

  ngOnInit() {}
}
