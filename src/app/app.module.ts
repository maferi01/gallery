import { LayoutModule } from '@angular/cdk/layout';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatSidenavModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule,
} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { TOKEN_MOCK } from 'projects/mycomps/src/lib/mockutil/mock-data.service';
import { MycompsModule } from 'projects/mycomps/src/lib/mycomps.module';

import { AppComponent } from './app.component';
import { CardFlexComponent } from './card-flex/card-flex.component';
import { ChildSubsComponent } from './child-subs/child-subs.component';
import { ChildComponent } from './child/child.component';
import { ChilddirDirective } from './childdir.directive';
import { CollectionComponent } from './collection/collection.component';
import { mockDataPart } from './data/mockdata.part';
import { FirstPageComponent } from './first-page/first-page.component';
import { LayoutTestComponent } from './layout-test/layout-test.component';
import { MaterialTableComponent } from './material-table/material-table.component';
import { MyCardComponent } from './my-card/my-card.component';
import { MyFormComponent } from './my-form/my-form.component';
import { MyFxLayoutComponent } from './my-fx-layout/my-fx-layout.component';
import { MyLayoutRenderComponent } from './my-layout-render/my-layout-render.component';
import { MyNavComponent } from './my-nav/my-nav.component';
import { ObservComponent } from './observ/observ.component';
import { SeconPageComponent } from './secon-page/secon-page.component';
import { SecureTextDirective } from './secure-text.directive';
import { StopClDirective } from './stop-cl.directive';

const appRoutes: Routes = [
  { path: 'first-page', component: FirstPageComponent },
  { path: 'second-page', component: SeconPageComponent },
  { path: 'third-page', component: MyFormComponent },
  { path: 'fx-page', component: MyFxLayoutComponent },
  { path: 'table', component: MaterialTableComponent },
  { path: 'layoutest', component: LayoutTestComponent },
  { path: 'observ', component: ObservComponent },


  ];

@NgModule({
  declarations: [
    AppComponent,
    MyNavComponent,
    FirstPageComponent,
    SeconPageComponent,
    MyCardComponent,
    CardFlexComponent,
    MyFormComponent,
    CollectionComponent,
    MyFxLayoutComponent,
    StopClDirective,
    MaterialTableComponent,
    MyLayoutRenderComponent,
    LayoutTestComponent,
    ChildComponent,
    ChilddirDirective,
    ObservComponent,
    SecureTextDirective,
    ChildSubsComponent
  ],
  imports: [
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatMenuModule,
    RouterModule.forRoot(appRoutes),
    FlexLayoutModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MycompsModule
    ],
    providers: [ { provide: TOKEN_MOCK, useValue: mockDataPart },
      ],
  bootstrap: [MyNavComponent]}
)
export class AppModule { }
